<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRenthistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('renthistory', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('rented_vehicle_id');
      $table->integer('renter_id');
      $table->integer('status');
      $table->timestamp('created_at');
      $table->timestamp('updated_at');
  });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('renthistory');
    }
}
