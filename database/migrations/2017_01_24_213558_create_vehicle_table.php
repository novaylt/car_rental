<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
      Schema::create('vehicles', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name');
      $table->string('type');
      $table->string('size');
      $table->integer('doors');
      $table->integer('capacity');
      $table->string('transmission');
      $table->string('colour');
      $table->integer('status');
      $table->timestamp('created_at');
      $table->timestamp('updated_at');
  });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::drop('vehicles');
    }
}
