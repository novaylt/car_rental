<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestCarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

     public function up()
    {
      Schema::create('requests', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('requester_id');
      $table->string('type');
      $table->integer('doors');
      $table->integer('capacity');
      $table->string('transmission');
      $table->string('colour');
      $table->timestamp('created_at');
      $table->timestamp('updated_at');
  });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::drop('requests');
    }
}
