<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscussioncontentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('discussioncontents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_thread_id');
            $table->integer('customer_id');
            $table->string('message');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('discussioncontents');
    }
}
