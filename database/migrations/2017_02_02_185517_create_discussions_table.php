<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscussionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('discussionslist', function (Blueprint $table) {
          $table->increments('id');
          $table->string('customer_id');
          $table->timestamp('created_at');
          $table->timestamp('updated_at');
          
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('discussionslist');
    }
}
