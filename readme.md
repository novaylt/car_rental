# Car Rental System
Car Rental System is a web application developed using the Laravel MVC Framework. The aim of this project was to design and develop a system that can theoritically work in a busy office. Its features are the following: 
•	Implement a data sharing functionality, 
•	Allow users to send and receive messages, 
•	Allow users to send and receive audio, video and image attachments,
•	Store communications in a database,
•	Allow customers to request a vehicle to rent,
•	Allow admin users to assign rent to a customer.
 

## Contact Me
If you have any questions regarding this project, please email me at [ibrahim_345@outlook.com].

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
