@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row" align="center">

                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-default">

                    <div class="panel-body">

                    


                @if(Session::has('errormessage'))
                    <div class="alert alert-error">
                        {{ Session::get('errormessage') }}
                    </div>
                @endif

                      <h2><i class="fa fa-star" aria-hidden="true"></i></i> RENT CAR </h2>
                      <br></br>

  
                    <br></br>


                      {!! Form::open() !!}
                  
                      <!-- Title form input -->
               

                              <div class="form-group">
                                {!! Form::Label('customer', 'Customer:') !!}
                                {!! Form::select('customer', $Customers, null) !!}
                              </div>
               


                                <div class="form-group">
                                {!! Form::label('paymenttype', 'Payment Type:') !!}
                                {!! Form::select('paymenttype', ['Choose an option' => "", 'Daily' => 'Daily', 'Weekly' => 'Weekly', 'Monthly' => 'Monthly']) !!}
                                </div>

<br></br>
           
                             <div class="form-group">
                                {!! Form::label('rent_start', 'Start Date:') !!}
                                {!! Form::text('rent_start', null) !!}
                             </div>

                             <div class="form-group">
                                {!! Form::label('rent_end', 'End Date:') !!}
                                {!! Form::text('rent_end', null) !!}
                             </div>
          

                              {{ Form::submit('SAVE', array('class' => 'btn btn-info')) }}

                              {!! Form::close() !!}


                              </div>


                    </div>

       <button class="btn btn-info" onclick="history.go(-1)">
                       « Return Back
       </button>

                </div>


            </div>

    </div>
</div>
@endsection
