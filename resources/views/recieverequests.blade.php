@extends('layouts.main')

@section('content')

    <div class="row"  align="center">

        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
    @section('contentheader')        
    <h2><i class="fa fa-exclamation-circle"></i> Recieved Requests</h2>
    @endsection
                            <div class="panel-body">
                                    
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped">

                                              @if(Session::has('delete_message'))
                                                          <div class="alert alert-success">
                                                              {{ Session::get('delete_message') }}
                                                          </div>
                                                      @endif

                                                      @yield('content')

                                                <thead>
                                                    <tr>
                                                        <th>Customer Name</th>
                                                        <th>Email</th>
                                                        <th>Recieved At</th>
                                                        <th>Manage</th>
                                                    </tr>
                                                </thead>
                                                @foreach($CarRequest as $key => $requested)
                                                <tr>
                                                  <td>{{ $requested->requestername }}</th>
                                                  <td>{{ $requested->requesteremail }}</th>
                                                  <td>{{ $requested->created_at }}</th>
                                                  <td><form action="/recieverequests/delete/{{ $requested->id }}" method="POST">
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}
                                                        
                                                        <a href="/recieverequests/{{ $requested->id }}" class ='btn btn-info'>View</a>
                                                        <button type="submit" class="btn btn-danger btn-mini">Delete</button></th>
                                                </tr>
                                                @endforeach

                                              </table>

                                        </div>

        </div>
    </div>
</div>
</div>

@endsection
