@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row" align="center">

      <div class="col-md-12 col-md-offset-0">
         
@section('contentheader')
             <div align="center"><h2><i class="fa fa-car"></i> ADD NEW VEHICLE</h2> </div>
          @endsection
      </div>



<div class="col-md-6 col-md-offset-0">
            <div class="panel panel-default">

                <div class="panel-heading"><i class="fa fa-info-circle"></i>  Vehicle Cover </div>
                    <div class="panel-body">

              
                           {!! Form::open(['action'=>'VehicleController@store', 'files'=>true]) !!}
                         
                        <div class="form-group">
                              {!! Form::label('Vehicle Image') !!}
                              {!! Form::file('cover', null) !!}  

                                <br></br>
                        </div>

                         <div class="col-md-12 col-md-offset-0">
                            {!! Form::label('daily_rate', 'Daily Rate (£):') !!}
                            {!! Form::text('daily_rate', null, ['class' => 'form-control']) !!}

                            </br>
                        </div>

                         <div class="col-md-12 col-md-offset-0">
                            {!! Form::label('weekly_rate', 'Weekly Rate (£):') !!}
                            {!! Form::text('weekly_rate', null, ['class' => 'form-control']) !!}
                            </br>
                        </div>

                          <div class="col-md-12 col-md-offset-0">
                            {!! Form::label('monthly_rate', 'Monthly Rate (£):') !!}
                            {!! Form::text('monthly_rate', null, ['class' => 'form-control']) !!}
                            </br>
                        </div>

                    
                  </div>
              </div>

                          @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                          @endif
</div>



        <div class="col-md-6 col-md-offset-0">
            <div class="panel panel-default">
   <div class="panel-heading"><i class="fa fa-info-circle"></i>  Vehicle Details </div>
                <div class="panel-body">
                 

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if(Session::has('successrequest'))
                            <div class="alert alert-success">
                                {{ Session::get('successrequest') }}
                            </div>
                        @endif

                   

         
                        <!-- Content form input -->
                        <div class="col-md-12 col-md-offset-0">
                            {!! Form::label('name', 'Vehicle Name:') !!}
                            {!! Form::text('name', null, ['class' => 'form-control']) !!}

                            </br>
                        </div>

                        <div class="form-group">
                            {!! Form::label('type', 'Vehicle Type:') !!}
                            {!! Form::select('type', ['' => '', 'Cars' => 'Cars', 'Vans' => 'Vans', 'SUVs' => 'SUVs']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('size', 'Vehicle Size:') !!}
                            {!! Form::select('size', ['' => '','Small' => 'Small', 'Medium' => 'Medium', 'Large' => 'Large']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('doors', 'No. Doors:') !!}
                            {!! Form::select('doors', ['' => '','2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('capacity', 'Passenger Capacity:') !!}
                            {!! Form::select('capacity', ['' => '','2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('transmission', 'Transmission Type:') !!}
                            {!! Form::select('transmission', ['' => '','Manual' => 'Manual', 'Automatic' => 'Automatic']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('colour', 'Vehicle Colour:') !!}
                             {!! Form::select('colour', ['' => '','Red' => 'Red', 'Blue' => 'Blue', 'Yellow' => 'Yellow', 'Green' => 'Green', 'Orange' => 'Orange',
                            'Black' => 'Black', 'White' => 'White', 'Grey' => 'Grey']) !!}
                        </div>
                
        
                </div>
               
                </br>


              </div>
      </div>


 <div class="col-md-2 col-md-offset-5">
            
               
<br></br>
                 {{ Form::submit('ADD VEHICLE', array('class' => 'btn btn-info')) }}
                {!! Form::close() !!}
               </br>
   
</div>
    </div>
</div>
@endsection
