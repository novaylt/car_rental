@extends('layouts.main')

@section('content')

    <div class="row" align="center">

    <div class="col-md-12 col-md-offset-0">
        <div class="panel panel-default">
@section('contentsection')
            <h2><i class="fa fa-book"></i> VEHICLES SEARCH RESULTS</h2>
@endsection         
        </div>
    </div>


 <div class="col-md-12 col-md-offset-0">
                 <div class="panel panel-default">
           
 
                  <div class="table-responsive">
                    <table class="table table-bordered table-striped">




                        


            @if (count($searchvehicles) === 0)

               <p><strong><h3>No Results Found!</h3></strong></p>
              
                @elseif (count($searchvehicles) >= 1)


                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Size</th>
                                <th>Doors</th>
                                <th>Capacity</th>
                                <th>Transmission</th>
                                <th>Colour</th>
                                <th>Status</th>
                            </tr>
                        </thead>

                <p><strong><h3>@if($countresults == 1)
                                     {{$countresults}} Result Found!
                                @elseif ($countresults > 1 )
                                    {{$countresults}} Results Found!
                                @endif

                                     </h3></strong></p>

                     @foreach($searchvehicles as $searchresults)  
                     <tr>
                          <td><strong><a href="#{{ $searchresults->id }}" class="portfolio-link" data-toggle="modal">{{ $searchresults->name }}</a></strong></th>
                          <td>{{$searchresults->type}}</th>
                          <td>{{$searchresults->size}}</th>
                          <td>{{$searchresults->doors}}</th>
                          <td>{{$searchresults->capacity}}</th>
                          <td>{{$searchresults->transmission}}</th>
                          <td>{{$searchresults->colour}}</th>
                          <td>@if($searchresults->status ==1 ) 
                                <a class ='btn btn-danger' href="#{{ $searchresults->name }}" class="portfolio-link" data-toggle="modal">RENTED</a>
                              @elseif($searchresults->status ==0 ) 
                                <a class ='btn btn-info'>AVAILABLE</a>
                              @endif</th>
                        </tr>

                 @endforeach
                @endif

             

                      </table>
         {!! $searchvehicles->appends(Request::except("page"))->render() !!} 
                </div>

 @foreach($searchvehicles as $key => $searchresults)

          <div class="portfolio-modal modal fade" id="{{ $searchresults->id }}"  tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-content">
                  <div class="close-modal" data-dismiss="modal">
                      <div class="lr">
                          <div class="rl">
                          </div>
                      </div>
                  </div>
                  <div class="container">
                      <div class="row">
                          <div class="col-lg-8 col-lg-offset-2">
                              <div class="modal-body" align="center">
                                  <!-- Cover Details Go Here -->
                                  <h2>Vehicle Name: <strong>{{ $searchresults->name }}</strong></h2>

                                  <img class="img-responsive img-centered" src="/images/{{ $searchresults->id }}.jpg" alt="">
                                       
                                  <ul class="list-inline">

                                    <h4><strong>Number Of Doors: {{ $searchresults->doors }}</strong></h4></li>
                                    <h4><strong>Size: {{ $searchresults->size }}</strong></h4></li>
                                    <h4><strong>Passengers: {{ $searchresults->capacity }}</strong></h4></li>
                                    <h4><strong>Transmission Type: {{ $searchresults->transmission }}</strong></h4></li>
                                    <h4><strong>Colour: {{ $searchresults->colour }}</strong></h4></li>

                                  </ul>
                                  <br><br>
                                  <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

 @endforeach


@foreach($Renter as $key => $rentprofile)

          <div class="portfolio-modal modal fade" id="{{ $rentprofile->vehiclename }}"  tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-content">
                  <div class="close-modal" data-dismiss="modal">
                      <div class="lr">
                          <div class="rl">
                          </div>
                      </div>
                  </div>
                  <div class="container">
                      <div class="row">
                          <div class="col-lg-8 col-lg-offset-2">
                              <div class="modal-body" align="center">
                                  <!-- Cover Details Go Here -->
                                  <h2>Vehicle Name: <strong>{{ $rentprofile->vehiclename }}</strong></h2>

                                  <img class="img-responsive img-centered" src="/images/{{ $rentprofile->rentedvehicleid }}.jpg" alt="">
                                       
                                  <ul class="list-inline">

                                    <h2><strong>Rent By:</strong></h2>
                                    <br>
                                    <h4><strong>Renter Name: {{ $rentprofile->rentername }}</strong></h4></li>
                                    <h4><strong>Email: {{ $rentprofile->renteremail }}</strong></h4></li>
                                    <h4><strong>Member Since: {{ $rentprofile->joinedat }}</strong></h4></li>
                                    <br></br>
                                    <h4><strong>Rent Type: {{ $rentprofile->paymenttype }}</strong></h4></li>
                                    <h4><strong>Rent Start: <{{ $rentprofile->rentstart }}> - Rent End: <{{ $rentprofile->rentend }}></strong></h4></li>
                                  </ul>
                                  <br><br>
                                  <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

 @endforeach


            </div>
                    <button class="btn btn-primary" onclick="history.go(-1)">
                      « Return Back
                    </button>
        </div>
             
    </div>
  


@endsection
