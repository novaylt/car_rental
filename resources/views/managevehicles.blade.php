@extends('layouts.main')

@section('content')

    <div class="row"  align="center">

            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
             
             @section('contentheader')       
                    <div class="panel-heading"><i class="fa fa-database"></i> Cars On the Website Database </div>
             @endsection
                       <div class="panel-body">

                         @if(Session::has('delete_message'))
                             <div class="alert alert-success">
                                 {{ Session::get('delete_message') }}
                             </div>
                         @endif


                        @if(Session::has('vehicle_update_message'))
                            <div class="alert alert-success">
                                {{ Session::get('vehicle_update_message') }}
                            </div>
                        @endif
                      

                        @if(Session::has('repossesstatus'))
                            <div class="alert alert-success">
                                {{ Session::get('repossesstatus') }}
                            </div>
                        @endif

                        @if(Session::has('rent_update_message'))
                            <div class="alert alert-success">
                                {{ Session::get('rent_update_message') }}
                            </div>
                        @endif
                        

                         <div class="table-responsive">
                           <table class="table table-bordered table-striped" >

                            <thread>
                                 <tr align="center">
                                     <td><strong>Name</strong></th>
                                     <td><strong>Status</strong></th>
                                     <td><strong>Change</strong></th>
                                     <td><strong>Manage</strong></th>
     
                                 </tr>
                             </thread></div>
                             @foreach($ManageVehicles as $key => $vehicle)
                             <thread>
                                   <tr align="center">
                                     <td><strong><a href="#{{ $vehicle->id }}" class="portfolio-link" data-toggle="modal">{{ $vehicle->vehiclename }}</a></strong></th >
                                     <td>@if($vehicle->status ==1 ) 
                                            <a class ='btn btn-danger' href="#{{ $vehicle->vehiclename }}" class="portfolio-link" data-toggle="modal">RENTED</a>
                                          @elseif($vehicle->status ==0 ) 
                                             <a href ="rentcar/{{$vehicle->id}}" class ='btn btn-info'>AVAILABLE</a>
                                          @endif </th>
                                      <td>
                                      @if($vehicle->status == 1 ) 
                                          <a href ="managevehicles/edit/{{$vehicle->id}}" class ='btn btn-info'>Edit Vehicle</a>   
                                      @elseif($vehicle->status == 0 )
                                      <a href ="managevehicles/edit/{{$vehicle->id}}" class ='btn btn-info'>Edit Vehicle</a> 
                                      @endif
                                      </th>

                                     <td>
                                     {{ Form::open(['route' => ['managevehicles', $vehicle->id], 'method' => 'delete']) }}</th>
                                          <input type="hidden" name="_method" value="DELETE">
                                          <button type="submit"class="btn btn-danger btn-mini">Delete Vehicle</button>

                                     {{ Form::close() }} 

                                   </tr>
                                </thread>
                             @endforeach
                         </table>


                                     {!! $ManageVehicles->render() !!} 
   


    @foreach($ManageVehicles as $key => $vehicle)

          <div class="portfolio-modal modal fade" id="{{ $vehicle->id }}"  tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-content">
                  <div class="close-modal" data-dismiss="modal">
                      <div class="lr">
                          <div class="rl">
                          </div>
                      </div>
                  </div>
                  <div class="container">
                      <div class="row">
                          <div class="col-lg-8 col-lg-offset-2">
                              <div class="modal-body" align="center">
                                  <!-- Cover Details Go Here -->
                                  <h2>Vehicle Name: <strong>{{ $vehicle->vehiclename }}</strong></h2>

                                  <img class="img-responsive img-centered" src="/images/{{ $vehicle->id }}.jpg" alt="">
                                       
                                  <ul class="list-inline">

                                    <h4><strong>Number Of Doors: {{ $vehicle->doors }}</strong></h4></li>
                                    <h4><strong>Size: {{ $vehicle->size }}</strong></h4></li>
                                    <h4><strong>Passengers: {{ $vehicle->capacity }}</strong></h4></li>
                                    <h4><strong>Transmission Type: {{ $vehicle->transmission }}</strong></h4></li>
                                    <h4><strong>Colour: {{ $vehicle->colour }}</strong></h4></li>


                                  </ul>
                                  <br><br>
                                  <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Design</button>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

 @endforeach


   @foreach($ViewRenter as $key => $rentprofile)

          <div class="portfolio-modal modal fade" id="{{ $rentprofile->vehiclename }}"  tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-content">
                  <div class="close-modal" data-dismiss="modal">
                      <div class="lr">
                          <div class="rl">
                          </div>
                      </div>
                  </div>
                  <div class="container">
                      <div class="row">
                          <div class="col-lg-8 col-lg-offset-2">
                              <div class="modal-body" align="center">
                                  <!-- Cover Details Go Here -->
                                  <h2>Vehicle Name: <strong>{{ $rentprofile->vehiclename }}</strong></h2>

                                  <img class="img-responsive img-centered" src="/images/{{ $rentprofile->rentedvehicleid }}.jpg" alt="">
                                       
                                   <ul class="list-inline">

                                    <h2><strong>Rent By:</strong></h2>
                                    <br>
                                    <h4><strong>Renter Name: {{ $rentprofile->rentername }}</strong></h4></li>
                                    <h4><strong>Email: {{ $rentprofile->renteremail }}</strong></h4></li>
                                    <h4><strong>Member Since: {{ $rentprofile->joinedat }}</strong></h4></li>
                                    <br></br>
                                    <h4><strong>Rent Type: {{ $rentprofile->paymenttype }}</strong></h4></li>
                                    <h4><strong>Rent Start: <{{ $rentprofile->rentstart }}> - Rent End: <{{ $rentprofile->rentend }}></strong></h4></li>
                                    <a href ="managevehicles/editrentstatus/{{$rentprofile->rentstatusid}}/{{$rentprofile->rentedvehicleid}}" class ='btn btn-warning'>Reposses Car</a> 
                                    <a href ="managevehicles/editrent/{{$rentprofile->rentstatusid}}" class ='btn btn-info'>Edit Rent Details</a>
                                  </ul>
                                  <br><br>
                                  <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

 @endforeach




                        </div>


                    </div>
                </div>
                </div>
            </div>

@endsection
