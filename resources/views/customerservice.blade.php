@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-12 col-md-offset-0">
          <div class="panel panel-default">

            <h2>  <div class="panel-heading" align="center"><i class="fa fa-rss-square"></i> CUSTOMER SERVICE BOARD</div></h2>
             <div class="panel-body" align="center">
 
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                                                  
                            <thead>
                                <tr>
                                    <th>Service Desk Operator</th>
                                    <th>Options</th>
                                </tr>
                            </thead>  

                            <tr>
           
                             @foreach($CustomerServiceThread as $key => $servicedesk)
                                     
                              <td><strong>ADMIN</strong></th>
                              <td> <a href ="customerservice/{{$servicedesk->id}}" class ='btn btn-info'>Enter Conversation</a></th> 
                            </tr>
                                  
                          @endforeach


 
                      </table>
                     
                </div>        
          </div>
      </div>
  </div>
</div>
@endsection
