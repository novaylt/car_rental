@extends('layouts.main')

@section('content')

    <div class="row">

        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">

                    <div class="panel-body" >
                    @section('contentheader')
                                        <h2><i class="fa fa-envelope"></i> Recieved Contact Messages</h2>
                    @endsection                    
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped"  align="center">

                                              @if(Session::has('delete_message'))
                                                          <div class="alert alert-success">
                                                              {{ Session::get('delete_message') }}
                                                          </div>
                                                      @endif

                                                      @yield('content')

                                                <thead>
                                                    <tr>
                                                        <th>Sent By</th>
                                                        <th>Email</th>
                                                        <th>Recieved At</th>
                                                        <th>Manage Message</th>
                                                    </tr>
                                                </thead>


      @if (count($Messages))                                                
                                       
                                                @foreach($Messages as $key => $message)
                                                <tr>
                                                  <td>{{ $message->name }}</th>
                                                  <td>{{ $message->email }}</th>
                                                  <td>{{ $message->created_at }}</th>
                                                  <td>  <form action="/messages/delete/{{ $message->id }}" method="POST">
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}
                                                        
                                                        <a href="/messages/{{ $message->id }}" class ='btn btn-info'>View</a>
                                                        <button type="submit" class="btn btn-danger btn-mini">Delete</button></th>
                                                    </tr>
                                                @endforeach
  @else  
         <div align="center"><p><strong><h3>YOU DO NOT HAVE ANY MESSAGES!</h3></strong></p></div>
  @endif    
                                        </table>

                                        </div>
 

        </div>

    </div>
</div>
</div>
@endsection
