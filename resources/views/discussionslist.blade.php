@extends('layouts.main')

@section('content')

    <div class="row">

        <div class="col-md-12 col-md-offset-0">
          <div class="panel panel-default">

@section('contentheader')
            <h2>  <div class="panel-heading" align="left"><i class="fa fa-rss-square"></i> CUSTOMER DISCUSSION BOARD</div></h2>
@endsection
             <div class="panel-body" align="center">

                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                                                  
                            <thead>
                                <tr>
                                    <th>@if($TotalDiscussions == 1)
                                         {{ $TotalDiscussions}} Customer
                                      @elseif ($TotalDiscussions > 1 )
                                         {{ $TotalDiscussions}} Customers
                                      @endif</th>
                                    <th>Email On</th>
                                    <th>Created On</th>
                                    <th>Options</th>
                                </tr>
                            </thead>  

                            <tr>
      @if (count($Discussions))
                             @foreach($Discussions as $key => $CustomerThreads)
                                     
                              <td><strong>{{ $CustomerThreads->customer_name }}</strong></th>
                              <td>{{ $CustomerThreads->customer_email  }}</th> 
                              <td>{{ $CustomerThreads->joinedat  }}</th> 
                              <td> <a href ="discussionslist/{{$CustomerThreads->id}}" class ='btn btn-info'>Enter Conversation</a></th> 
                            </tr>
                                  
                          @endforeach

        @else  
        <p><strong><h3>No Threads Exists Available Yet!</h3></strong></p>
       @endif   
 
                      </table>
                          {!! $Discussions->render() !!} 
                </div>        
          </div>
      </div>

</div>
@endsection
