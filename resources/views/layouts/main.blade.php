<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <title>CR - Car Rentals</title>

 <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/skins/skin-blue.min.css">
    <link rel="stylesheet" href="/css/dist/css/AdminLTE.min.css">
    <!-- Bootstrap 3.3.2 -->
    <link rel="stylesheet" href="/css/bootstrap/css/bootstrap.min.css"">
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->

    <!-- Bootstrap Core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- JavaScripts -->
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}


    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script src="/js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script src="/js/jquery.smoothdivscroll-1.3-min.js" type="text/javascript"></script>
    <script src="/js/jqueryzoomimg.js" type="text/javascript"></script>
  <!--   <script src="/js/jquerycalender.js" type="text/javascript"></script> -->


  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.12.4.js"></script>
  <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

</head>


    <body class="skin-blue">
    <div class="wrapper">

        <!-- Main Header -->
        <header class="main-header">

            <!-- Logo -->

                    @if (Auth::guest())
                    <a class="logo" href="{{ url('/login') }}"><i class="fa fa-home"></i> Car-Rental</a>
                      @elseif (Auth::user()->admin!=1)
                    <a class="logo" href="{{ url('/dashboard') }}"><i class="fa fa-home"></i> DASHBOARD</a>
                      @elseif(Auth::user()->admin==1)
                    <a class="logo" href="{{ url('/dashboard') }}"><i class="fa fa-home"></i> DASHBOARD</a>
                    @endif


            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- Tasks Menu -->
                        <li class="dropdown tasks-menu">
                            <!-- Menu Toggle Button -->
                            @if (Auth::guest())  
                            @elseif(Auth::user()->admin==1)    
                            <a href="messages"><i class="fa fa-envelope-o"> Messages</i> </a>
                            @elseif(Auth::user()->admin!=1)    
                            
                           @endif
                        </li>

                         <!-- User Account Menu -->
                        @if (Auth::guest())  
                          <li><a href="{{ url('/login') }}"><i class="fa fa-sign-in"></i> Login</a></li>
                          <li><a href="{{ url('/register') }}"><i class="fa fa-book"></i> Register</a></li>
                       @else

                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span><i class="fa fa-user"></i> <u> {{ Auth::user()->name }}</u></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                    <p>
                                        {{ Auth::user()->name }} <br> 
                                        {{ Auth::user()->email }}</br></br>
                                        @if(Auth::user()->admin==1) 
                                        ADMIN USER
                                        @elseif(Auth::user()->admin!=1) 
                                        STANDARD USER
                                        @endif
                                        <small>Member Since {{ Auth::user()->created_at }}</small>
                                    </p>
                                </li>

                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="/profile" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="/logout" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        @endif
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">

            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">

                <!-- /.search form -->

                <!-- Sidebar Menu -->
                <ul class="sidebar-menu">


                    <!-- Optionally, you can add icons to the links -->
                     <li class="header"><i class="fa fa-bars"></i> NAVIGATION PANEL</li>
                     @if (Auth::guest())
                    <li><a href="{{ url('/contact') }}"><i class="fa fa-envelope-square"></i> Contact Admin</a></li>
                     @elseif (Auth::user()->admin!=1 & Auth::user()->ban_status!=1)
                    <li><a href="{{ url('/request') }}"><i class="fa fa-car"></i> Request Car</a></li>
                    <li><a href="{{ url('/contact') }}"><i class="fa fa-envelope-square"></i> Contact Admin</a></li>
                    <li><a href="{{ url('/customerservice') }}"><i class="fa fa-rss-square"></i> Customer Service</a></li>
                    @elseif (Auth::user()->admin!=1 & Auth::user()->ban_status==1)
                    <li><a href="{{ url('/contact') }}"><i class="fa fa-envelope-square"></i> Contact Admin</a></li>
                     @elseif(Auth::user()->admin==1)
                    <li><a href="{{ url('/vehicles') }}"><i class="fa fa-car"></i> All Vehicles</a></li>
                    <li class="treeview">
                        <a href="#"><i class="fa fa-info"></i><span>Management</span> <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="{{ url('addvehicle') }}"><i class="fa fa-plus"></i> Add Vehicle</a></li>
                            <li><a href="{{ url('managevehicles') }}"><i class="fa fa-car"></i> Manage Vehicle</a></li>   
                            <li><a href="{{ url('manageusers') }}"><i class="fa fa-users"></i> Manage Users</a></li>       
                        </ul>
                    </li>
                    <li><a href="{{ url('/recieverequests') }}"><i class="fa fa-retweet"></i></i> Recieved Requests</a></li>
                    <li><a href="{{ url('/discussionslist') }}"><i class="fa fa-rss-square"></i> Chat Customers</a></li>
                    <li><a href="{{ url('/dashboard') }}"><i class="fa fa-bar-chart"></i> Statistics</a></li>   

                </ul><!-- /.sidebar-menu -->
                @endif
            </section>
            <!-- /.sidebar -->
        </aside>

    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                   @yield('contentheader')
                </h1>  
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Your Page Content Here -->
                @yield('content')

            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <!-- Main Footer -->
        <footer class="main-footer">
           
            <!-- Default to the left -->
           @include('footer')
        </footer>

    </div><!-- ./wrapper -->


    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.3 -->
    <script src="{{ asset ("/css/plugins/jQuery/jquery-2.2.3.min.js") }}" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset ("/css/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset ("/css/dist/js/app.min.js") }}" type="text/javascript"></script>

    </body>
</html>