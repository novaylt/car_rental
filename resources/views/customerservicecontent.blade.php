@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row" align="center">
        <div class="row" align="left">
                <div class="col-md-7 col-md-offset-0">
                    <div class="panel panel-default">
                        <div class="panel-heading" align="center"><h2><i class="fa fa-commenting-o"></i> SERVICE DESK DISCUSSION <th></h2></div>
                            <div class="panel-body">

                              @if (count($errors) > 0)
                                  <div class="alert alert-danger">
                                      <ul>
                                          @foreach ($errors->all() as $error)
                                              <li>{{ $error }}</li>
                                          @endforeach
                                      </ul>
                                  </div>
                              @endif


                        @if(Session::has('reply_success'))
                            <div class="alert alert-success">
                                {{ Session::get('reply_success') }}
                            </div>
                        @endif

                <article>

@if (count($threadconvo))
               @foreach($threadconvo as $conversation)

                                            <p><i class="fa fa-check"></i><small> Sent by <<b>

                                                  @if ($conversation->sendername ==  Auth::user()->name)
                                                        You
                                                  @elseif ($conversation->sendername !=  Auth::user()->name)
                                                       Admin
                                                  @endif


                                                </b>> 
                                                At <<b>{{$conversation->created_at}}</b>></small>
                                                @if ($conversation->attachments == null)
                                                @elseif ($conversation->attachments != null)
                                                <a href="#image{{ $conversation->messageid }}" class="portfolio-link" data-toggle="modal"><i class="fa fa-paperclip"></i>Image Attachment</a>
                                                @endif
                                                @if($conversation->videoattachment == null)
                                                @elseif ($conversation->videoattachment != null)
                                                <a href="#video{{ $conversation->messageid }}" class="portfolio-link" data-toggle="modal"><i class="fa fa-paperclip"></i>Video Attachment</a>
                                                @endif

                                                @if ($conversation->audioattachment == null)
                                                @else ($conversation->audioattachment != null)
                                                <div align="left">
                                                       <audio controls>
                                                        <source src="/attachments/{{ $conversation->messageid }}.mp3" type="audio/mpeg">
                                                      </audio> 
                                                 </div>
                                               <br><br>
                                                @endif

                                                <p> {{$conversation->message}}
                                                <div align = "center">-----------------------------------------------------------------------------------------------------------------------------</div>
                                        <br>
                                 
                              </article>

                @endforeach

  @else  
         <div align="center"><p><strong><h3>Start The Thread!</h3></strong></p></div>
  @endif 

              <div align="center">
                 {!! $threadconvo->render() !!} 
              </div>



              </div>
            </div>
          </div>
<div class="col-md-4 col-md-offset-0">
            <div class="panel panel-default" align="center">

                     <h4>  <div class="panel-heading" align="center"><i class="fa fa-info-circle"></i>  Submit A Reply</div></h4>
                    <div class="panel-body">
    
                     @if (count($errors) > 0)
                          <div class="alert alert-danger">
                              <ul>
                                  @foreach ($errors->all() as $error)
                                      <li>{{ $error }}</li>
                                  @endforeach
                              </ul>
                          </div>
                      @endif


                        @if(Session::has('customer_reply_success'))
                              <div class="alert alert-success">
                                  {{ Session::get('customer_reply_success') }}
                              </div>
                          @endif

                      

         {!! Form::open(['action'=>'DiscussionController@createcustomerreply', 'files'=>true]) !!}
        <!-- Content form input -->
            <div class="form-group">
              {!! Form::label('customerreply', 'Reply:') !!}
              {!! Form::textarea('customerreply', null, ['class' => 'form-control', 'rows' => 4, 'cols' => 40]) !!}
            </div>

            <div class="form-group">
                {!! Form::label('Upload an Image') !!}
                {!! Form::file('image', null) !!}
            </div>

            <br>

           <div class="form-group">
            {!! Form::label('audio', 'Upload Audio:') !!}
            {!! Form::file('audio', null) !!}
            
           </div>

           <br>

           <div class="form-group">
            {!! Form::label('video', 'Upload Video:') !!}
            {!! Form::file('video', null) !!}
            
           </div>


           <div align="center"> {{ Form::submit('Submit', array('class' => 'btn btn-info')) }} </div>

         {!! Form::close() !!}


  
                       </div>
                </div>
        </div>




 @foreach($threadconvo as $conversation)


          <div class="portfolio-modal modal fade" id="image{{ $conversation->messageid }}"  tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-content">
                  <div class="close-modal" data-dismiss="modal">
                      <div class="lr">
                          <div class="rl">
                          </div>
                      </div>
                  </div>
                  <div class="container">
                      <div class="row">
                          <div class="col-lg-8 col-lg-offset-2">
                              <div class="modal-body" align="center">
                                  <!-- Cover Details Go Here -->

                              <h4><strong>Image Attachment:</strong></h4></li>
                                  <img class="img-responsive img-centered" src="/attachments/{{ $conversation->messageid }}.jpg" alt="">
                                       
                                  <ul class="list-inline">
                                    <br><br>
                                    <h4><strong>Recieved At: {{ $conversation->created_at }}</strong></h4></li>

                                  </ul>
                                  <br><br>
                                  <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close </button>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>


 @endforeach




  @foreach($threadconvo as $conversation)


          <div class="portfolio-modal modal fade" id="video{{ $conversation->messageid }}"  tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-content">
                  <div class="close-modal" data-dismiss="modal">
                      <div class="lr">
                          <div class="rl">
                          </div>
                      </div>
                  </div>
                  <div class="container">
                      <div class="row">
                          <div class="col-lg-8 col-lg-offset-2">
                              <div class="modal-body" align="center">
                                  <!-- Cover Details Go Here -->


                                    <h4><strong>Video Attachment:</strong></h4></li>
                                        <div align="center">
                                               <video width="750" controls>
                                                  <source src="/attachments/{{ $conversation->messageid }}.3gp" type="video/3GP">
                                                  <source src="/attachments/{{ $conversation->messageid }}.ogg" type="video/ogg">
                                                  <source src="/attachments/{{ $conversation->messageid }}.mp4" type="video/mpeg">
                                                  <source src="/attachments/{{ $conversation->messageid }}.MP4" type="video/mp4">
                                                  <source src="/attachments/{{ $conversation->messageid }}.avi" type="video/avi">
                                                  <source src="/attachments/{{ $conversation->messageid }}.wav" type="video/wav">

                                                  Your browser does not support the video tag.
                                               
                                              </video> 
                                         </div>

                                    <ul class="list-inline">
                                      <br><br>
                                      <h4><strong>Recieved At: {{ $conversation->created_at }}</strong></h4></li>
                                    </ul>

                                  <br><br>
                                  <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close </button>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>


 @endforeach


        </div> 
    </div>    
</div>
@endsection
