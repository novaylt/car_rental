@extends('layouts.main')

@section('content')

    <div class="row" align="center">
        <div class="col-md-4 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><h1><i class="fa fa-info-circle"></i> REQUEST DETAILS <th></h1></div>
                          <div class="panel-body">
                               <div class="col-md-12 col-md-offset-0">
                                          <div class="panel panel-default">

                                              <div class="panel-heading">
                                                <div class="panel-body" align="left">
                                                     <p> <strong>Type:</strong>    {{ $requestinfo->type }} <p>
                                                         <strong>Doors:</strong>   {{ $requestinfo->doors }}<p>
                                                         <strong>Capacity:</strong>   {{ $requestinfo->capacity }}<p>
                                                         <strong>Transmission: </strong>  {{ $requestinfo->transmission }}<p>
                                                         <strong>Colour:</strong>   {{ $requestinfo->colour }} <p>
                                                </div>
                                              </div>
                                         </div>
                                </div>
                          </div>
            </div>

        </div>






         <div class="col-md-4 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><h1><i class="fa fa-info-circle"></i> SEARCH CRITERIA <th></h1></div>
                          <div class="panel-body">
                               <div class="col-md-12 col-md-offset-0">
                                          <div class="panel panel-default">

                                              <div class="panel-heading">
                                                <div class="panel-body" align="center">


                       <div class="col-md-1 col-md-offset-6">
                                <form id="custom-search-form" class="form-search form-horizontal pull-right" action="{{ URL::action('RequestsController@search') }}" method="get">
                                   
                                         <strong>Type: </strong><input type="text" class="search-query" value="{{ $requestinfo->type }}" name="typequery" placeholder="Search Type..." ><p>
                                         <strong>Doors: </strong><input type="text" class="search-query" value="{{ $requestinfo->doors }}" name="doorsquery" placeholder="Search Doors..." ><p>
                                         <strong>Capacity: </strong><input type="text" class="search-query" value="{{ $requestinfo->capacity }}" name="capacityquery" placeholder="Search Capacity..." ><p>
                                         <strong>Transmission: </strong><input type="text" class="search-query" value="{{ $requestinfo->transmission }}" name="transmissionquery" placeholder="Search Transmission..." ><p>
                                         <strong>Colour: </strong><input type="text" class="search-query" value="{{ $requestinfo->colour }}" name="colourquery" placeholder="Search Colour..." ><p>            
                                           
                                           <div class="form-group" align="center">
                                                <input class="btn btn-primary" type="submit" value="Search">
                                            </div> 
                                </form>   
                        </div>


                                                </div>
                                              </div>
                                         </div>
                                </div>
                          </div>
            </div>
     
        </div>




    </div>

@endsection
