@extends('layouts.main')

@section('content')

    <div class="row" align="center">

      <div class="col-md-6 col-md-offset-3">
          <div class="panel panel-default">

              <div class="panel-heading">   Want A Fancy Car To Rent? Make A Request!</div>
          </div>
      </div>

@section('contentheader')
  <h2><i class="fa fa-car"></i> REQUEST A CAR</h2>
@endsection
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">

                <div class="panel-body">
                
                  @if (count($errors) > 0)
                      <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif

                  @if(Session::has('successrequest'))
                      <div class="alert alert-success">
                          {{ Session::get('successrequest') }}
                      </div>
                  @endif

                  {!! Form::open() !!}


                  <!-- Content form input -->
                  <div class="form-group">
                      {!! Form::label('type', 'Vehicle Type:') !!}
                      {!! Form::select('type', ['' => '', 'Cars' => 'Cars', 'Vans' => 'Vans', 'SUVs' => 'SUVs']) !!}
                  </div>

                  <div class="form-group">
                      {!! Form::label('doors', 'Number Of Doors:') !!}
                      {!! Form::select('doors', ['' => '','2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6']) !!}
                  </div>

                  <div class="form-group">
                      {!! Form::label('capacity', 'Passenger Capacity:') !!}
                      {!! Form::select('capacity', ['' => '','2' => '2', 
                      '4' => '4', '5' => '5', '6' => '6']) !!}
                  </div>

                  <div class="form-group">
                      {!! Form::label('transmission', 'Transmission Type:') !!}
                      {!! Form::select('transmission', ['' => '','Manual' => 'Manual', 'Automatic' => 'Automatic']) !!}
                  </div>

                  <div class="form-group">
                      {!! Form::label('colour', 'Vehicle Colour:') !!}
                       {!! Form::select('colour', ['' => '','Red' => 'Red', 'Blue' => 'Blue', 'Yellow' => 'Yellow', 'Green' => 'Green', 'Orange' => 'Orange',
                      'Black' => 'Black', 'White' => 'White', 'Grey' => 'Grey']) !!}
                  </div>


                {{ Form::submit('REQUEST', array('class' => 'btn btn-info')) }}

                {!! Form::close() !!}

                    </div>

                </div>
        </div>


      

    </div>

@endsection
