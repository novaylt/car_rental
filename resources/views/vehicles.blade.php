@extends('layouts.main')

@section('content')

    <div class="row"  align="center">

            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">

                       <div class="panel-body">

                              @if(Session::has('vehicleaddedmessage'))
                                  <div class="alert alert-success">
                                      {{ Session::get('vehicleaddedmessage') }}
                                  </div>
                              @endif

                              @if(Session::has('rentsaved'))
                                  <div class="alert alert-success">
                                      {{ Session::get('rentsaved') }}
                                  </div>
                              @endif

@section('contentheader')
                                        <h2><i class="fa fa-car"></i> ALL VEHICLES</h2>
@endsection
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped">

                                                <thead>
                                                    <tr>
                                                        <th><strong>Name</strong></th>
                                                        <th><strong>Type</strong></th>
                                                        <th><strong>Size</strong></th>
                                                        <th><strong>Doors</strong></th>
                                                        <th><strong>Passengers</strong></th>
                                                        <th><strong>Transmission</strong></th>
                                                        <th><strong>Colour</strong></th>
                                                        <th><strong>Daily Rate</strong></th>
                                                        <th><strong>Weekly Rate</strong></th>
                                                        <th><strong>Monthly Rate</strong></th>
                                                        <th><strong>Status</strong></th>
                                                    </tr>
                                                </thead></div>
                                                @foreach($Vehicles as $key => $allvehicles)
                                                <tr>
                                                  <td><a href="#{{ $allvehicles->id }}" class="portfolio-link" data-toggle="modal">{{ $allvehicles->name }}</a></th>
                                                  <td>{{ $allvehicles->type }}</th>
                                                  <td>{{ $allvehicles->size }}</th>
                                                  <td>{{{$allvehicles->doors }}}</th>
                                                  <td>{{ $allvehicles->capacity }}</th>
                                                  <td>{{ $allvehicles->transmission }}</th>
                                                  <td>{{ $allvehicles->colour }}</th></th>
                                                  <td>£{{ $allvehicles->dailyrate }}</th>
                                                  <td>£{{ $allvehicles->weeklyrate }}</th>
                                                  <td>£{{ $allvehicles->monthlyrate }}</th></th>
                                                  <td>@if($allvehicles->status ==1 ) 
                                                        <a class ='btn btn-danger' href="#{{ $allvehicles->name }}" class="portfolio-link" data-toggle="modal">RENTED</a>
                                                      @elseif($allvehicles->status ==0 ) 
                                                        <a href ="rentcar/{{$allvehicles->id}}" class ='btn btn-info'>AVAILABLE</a>
                                                      @endif</th></th>
                                                </tr>
                                                @endforeach
                                              </table>

                                    {!! $Vehicles->render() !!} 
                                      </div>




 @foreach($Vehicles as $key => $allvehicles)

          <div class="portfolio-modal modal fade" id="{{ $allvehicles->id }}"  tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-content">
                  <div class="close-modal" data-dismiss="modal">
                      <div class="lr">
                          <div class="rl">
                          </div>
                      </div>
                  </div>
                  <div class="container">
                      <div class="row">
                          <div class="col-lg-8 col-lg-offset-2">
                              <div class="modal-body" align="center">
                                  <!-- Cover Details Go Here -->
                                  <h2>Vehicle Name: <strong>{{ $allvehicles->name }}</strong></h2>

                                  <img class="img-responsive img-centered" src="/images/{{ $allvehicles->id }}.jpg" alt="">
                                       
                                  <ul class="list-inline">

                                    <h4><strong>Number Of Doors: {{ $allvehicles->doors }}</strong></h4></li>
                                    <h4><strong>Size: {{ $allvehicles->size }}</strong></h4></li>
                                    <h4><strong>Passengers: {{ $allvehicles->capacity }}</strong></h4></li>
                                    <h4><strong>Transmission Type: {{ $allvehicles->transmission }}</strong></h4></li>
                                    <h4><strong>Colour: {{ $allvehicles->colour }}</strong></h4></li>

                                  </ul>
                                  <br><br>
                                  <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

 @endforeach



    @foreach($Renter as $key => $rentprofile)

          <div class="portfolio-modal modal fade" id="{{ $rentprofile->vehiclename }}"  tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-content">
                  <div class="close-modal" data-dismiss="modal">
                      <div class="lr">
                          <div class="rl">
                          </div>
                      </div>
                  </div>
                  <div class="container">
                      <div class="row">
                          <div class="col-lg-8 col-lg-offset-2">
                              <div class="modal-body" align="center">
                                  <!-- Cover Details Go Here -->
                                  <h2>Vehicle Name: <strong>{{ $rentprofile->vehiclename }}</strong></h2>

                                  <img class="img-responsive img-centered" src="/images/{{ $rentprofile->rentedvehicleid }}.jpg" alt="">
                                       
                                  <ul class="list-inline">

                                    <h2><strong>Rent By:</strong></h2>
                                    <br>
                                    <h4><strong>Renter Name: {{ $rentprofile->rentername }}</strong></h4></li>
                                    <h4><strong>Email: {{ $rentprofile->renteremail }}</strong></h4></li>
                                    <h4><strong>Member Since: {{ $rentprofile->joinedat }}</strong></h4></li>
                                    <br></br>
                                    <h4><strong>Rent Type: {{ $rentprofile->paymenttype }}</strong></h4></li>
                                    <h4><strong>Rent Start: <{{ $rentprofile->rentstart }}> - Rent End: <{{ $rentprofile->rentend }}></strong></h4></li>
                                  </ul>
                                  <br><br>
                                  <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

 @endforeach


        </div>
    </div>
</div>
</div>

@endsection
