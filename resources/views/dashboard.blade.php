@extends('layouts.main')

@section('content')
   
 <div class="row"  align="center">
@if(Auth::user()->admin==1)    


@section('contentheader')
               <h3><i class="fa fa-bar-chart" ></i> Statistics Dashboard</h3>
@endsection


 <div class="col-md-12 col-md-offset-0">
        <div class="col-md-3 col-md-offset-0">
        <br>
           <div class="info-box">
                  <!-- Apply any bg-* class to to the icon to color it -->
                  <span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Registered Customer(s)</span>
                    <span class="info-box-number"><h3><strong><a href ="/manageusers">{{ $TotalUsers}}</a></strong></h3></span>
                  </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->  
          </div>

          <div class="col-md-3 col-md-offset-0">
            <br>
           <div class="info-box">
                  <!-- Apply any bg-* class to to the icon to color it -->
                  <span class="info-box-icon bg-orange"><i class="fa fa-car"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Total Vehicle(s) </span>
                    <span class="info-box-number"><h3><strong><a href ="/vehicles">{{ $TotalVehicles}}</a></strong></h3></span>
                  </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->  
           </div>

          <div class="col-md-3 col-md-offset-0">
            <br>
           <div class="info-box">
                  <!-- Apply any bg-* class to to the icon to color it -->
                  <span class="info-box-icon bg-green"><i class="fa fa-retweet"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Total Request(s) </span>
                    <span class="info-box-number"><h3><strong><a href ="/recieverequests">{{ $TotalRequests}}</a></strong></h3></span>
                  </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->  
           </div>

          <div class="col-md-3 col-md-offset-0">
            <br>
           <div class="info-box">
                  <!-- Apply any bg-* class to to the icon to color it -->
                  <span class="info-box-icon bg-red"><i class="fa fa-envelope"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Total Contact Message(s) </span>
                    <span class="info-box-number"><h3><strong><a href ="/messages">{{ $TotalContactMessages}}</a></strong></h3></span>
                  </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->  
           </div>
</div>





    
           <div class="info-box">
     <div class="panel-body">  

              <div class="col-md-4 col-md-offset-0">
              <br></br>
                     <div class="panel-heading" align="center"><i class="fa fa-info-circle"></i> Total Database Population </div>
                                               <script type="text/javascript">
                                                google.charts.load("current", {packages:['corechart']});
                                                google.charts.setOnLoadCallback(drawChart);
                                                function drawChart() {
                                                  var data = google.visualization.arrayToDataTable([
                                                    ["Category", "Population", { role: "style" } ],
                                                    ["Total Members", {{ $TotalUsers}}, "#0000cc"],
                                                    ["Total Vehicles",  {{ $TotalVehicles}}, "#33cc33"],
                                                    ["Total Requests",  {{ $TotalRequests}}, "#e6e600"],
                                                    ["Total Messages", {{ $TotalContactMessages}}, "#e65c00"],
                                                    
                                                  ]);

                                                  var view = new google.visualization.DataView(data);
                                                  view.setColumns([0, 1,
                                                                   { calc: "stringify",
                                                                     sourceColumn: 1,
                                                                     type: "string",
                                                                     role: "annotation" },
                                                                   2]);

                                                  var options = {

                                                    width: 400,
                                                    height: 325,
                                                    bar: {groupWidth: "45%"},
                                                    legend: { position: "none" },

                                                  };
                                                  var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
                                                  chart.draw(view, options);
                                              }
                                              </script>

                                      <div id="columnchart_values" align="center"></div>
                </div>                   



             <div class="col-md-4 col-md-offset-0">
                           <br></br>
                         <div class="panel-heading" align="center"><i class="fa fa-info-circle"></i> Car Rental Percentage </div>
                                 <script type="text/javascript">
                                          google.charts.load('current', {'packages':['corechart']});
                                          google.charts.setOnLoadCallback(drawChart);
                                          function drawChart() {

                                            var data = google.visualization.arrayToDataTable([
                                              ['Status', 'Total'],
                                              ['Rented ',   {{ $RentedCars}}],
                                              ['Available',  {{ $AvailableCars}}],
     
                                            ]);

                                            var options = {
                                              title: ''

                                            };

                                            var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                                            chart.draw(data, options);
                                          }
                                 </script>
                                     
                                     
                          <div id="piechart" style="width: 500px; height: 325px;"></div>               
                 </div>    


                <div class="col-md-4 col-md-offset-0">
                              <br></br>
                         <div class="panel-heading" align="center"><i class="fa fa-info-circle"></i> Banned Customers Percentage </div>
                                <script type="text/javascript">
                                    google.charts.load("current", {packages:["corechart"]});
                                    google.charts.setOnLoadCallback(drawChart);
                                    function drawChart() {
                                      var data = google.visualization.arrayToDataTable([
                                        ['Status', 'Total'],
                                        ['Banned',     {{ $BannedUsers}}],
                                        ['Not Banned',   {{ $NotBannedUsers}}]
                                      ]);

                                      var options = {
                                        title: '',
                                        pieHole: 0.4,
                                      };

                                      var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
                                      chart.draw(data, options);
                                    }
                                  </script>
                                                                   
                                     
                          <div id="donutchart" style="width: 500px; height: 325px;"></div>               
                 </div>  
                 


  @elseif (Auth::user()->admin!=1 & Auth::user()->ban_status==1)


              <div class="col-md-8 col-md-offset-2">
                          <div class="panel panel-default">

                                  <div class="panel-heading"><i class="fa fa-info-circle"></i>     YOU ARE BANNED! </div>
                                  <div class="panel-body"> 
                                                  User {{ Auth::user()->name }}. The Admin has banned you from doing anything else on the website till furthur notice.<p>
                                                  The reasons for this could very well be due to one or many of the following reasons:<p>
                                                  - Abusing the discussions section</p>
                                                  - Use of profanity</p>
                                                  - Hacking</p>
                                                  - Questionable Transactions.</p>
                                                                                                
                                                  If you think that you have been banned for reasons not of your doing, then please contact the admin.

                                  </div>
                            
                    




@elseif(Auth::user()->admin!=1 & Auth::user()->ban_status!=1)


    <div class="col-md-12 col-md-offset-0">
        <div class="panel panel-default">
       @section('contentheader')     
            <h2><i class="fa fa-database"></i> Your Rent History </h2>
        @endsection    
               <div class="panel-body">

@if (count($RentHistory))
                 <div class="table-responsive">
                   <table class="table table-bordered table-striped" >

                    <thread>
                         <tr align="left">
                            <th><strong>Car</strong></th>
                            <th><strong>Size</strong></th>
                            <th><strong>Doors</strong></th>
                            <th><strong>Passengers</strong></th>
                            <th><strong>Transmission</strong></th>
                            <th><strong>Colour</strong></th>
                            <th><strong>Rent Start</strong></th>
                            <th><strong>Rent End</strong></th>
                            <th><strong>Payment</strong></th>
                            <th><strong>Submitted At</strong></th>
                         </tr>
                     </thread>
                     </div>
           
                     <thread>
                           <tr align="left">
                    @foreach($RentHistory as $key => $historyofrent)
                             <td><a href="#{{ $historyofrent->id }}" class="portfolio-link" data-toggle="modal">{{ $historyofrent->vehiclename }}</a></th>
                             <td>{{$historyofrent->size}}</th>
                             <td>{{$historyofrent->doors}}</th>
                             <td>{{$historyofrent->capacity}}</th>
                             <td>{{$historyofrent->transmission}}</th >
                             <td>{{$historyofrent->colour}}</th>
                             <td>{{$historyofrent->rentstart}}</th>
                             <td>{{$historyofrent->rentend}}</th>
                             <td>{{$historyofrent->paymenttype}}</th>
                             <td>{{$historyofrent->created_at}}</th>
                           </tr>
                    @endforeach
                        </thread>
                    
                 </table>

              <div align="center">
                 {!! $RentHistory->render() !!} 
              </div>

  @else  
         <div align="center"><p><strong><h3>YOU HAVE NOT RENTED ANY CARS YET!</h3></strong></p></div>
  @endif 


@foreach($RentHistory as $key => $vehicleinfo)

          <div class="portfolio-modal modal fade" id="{{ $vehicleinfo->id }}"  tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-content">
                  <div class="close-modal" data-dismiss="modal">
                      <div class="lr">
                          <div class="rl">
                          </div>
                      </div>
                  </div>
                  <div class="container">
                      <div class="row">
                          <div class="col-lg-8 col-lg-offset-2">
                              <div class="modal-body" align="center">
                                  <!-- Cover Details Go Here -->
                                  <h2>Vehicle Name: <strong>{{ $vehicleinfo->vehiclename }}</strong></h2>

                                  <img class="img-responsive img-centered" src="/images/{{ $vehicleinfo->id }}.jpg" alt="">
                                       
                                  <ul class="list-inline">

                                    <h4><strong>Number Of Doors: {{ $vehicleinfo->doors }}</strong></h4></li>
                                    <h4><strong>Size: {{ $vehicleinfo->size }}</strong></h4></li>
                                    <h4><strong>Passengers: {{ $vehicleinfo->capacity }}</strong></h4></li>
                                    <h4><strong>Transmission Type: {{ $vehicleinfo->transmission }}</strong></h4></li>
                                    <h4><strong>Colour: {{ $vehicleinfo->colour }}</strong></h4></li>

                                  </ul>
                                  <br><br>
                                  <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

 @endforeach


        </div>
    </div>                      

@endif

                </div>
            </div>
        </div>

@endsection
