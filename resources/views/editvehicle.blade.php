@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row" align="center">

        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">

                <div class="panel-heading"><h2> <i class="fa fa-plus"></i> Edit Vehicle</h2></div>
                <div class="panel-body">


<div class="col-md-4 col-md-offset-0">
            <div class="panel panel-default">

                <div class="panel-heading"><i class="fa fa-info-circle"></i>  Vehicle Cover </div>
                    <div class="panel-body">

              
                   {!! Form::open() !!}
                  <!-- Title form input -->
                  <div class="form-group">
                      {!! Form::label('Vehicle Cover') !!}
                      <img class="img-responsive img-centered" src="/images/{{ $data->id }}.jpg" alt="">
                       {!! Form::file('cover', null) !!}
                </div>

              

                    </div>
                </div>

                  @if (count($errors) > 0)
                      <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif
        </div>


        <div class="col-md-6 col-md-offset-2">
            <div class="panel panel-default">
   <div class="panel-heading"><i class="fa fa-info-circle"></i>  Vehicle Details </div>
                <div class="panel-body">
                 

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if(Session::has('successrequest'))
                            <div class="alert alert-success">
                                {{ Session::get('successrequest') }}
                            </div>
                        @endif

                   

         
                        <!-- Content form input -->
                        <div class="col-md-12 col-md-offset-0">
                            {!! Form::label('name', 'Vehicle Name:') !!}
                            {!! Form::text('name', $data->name, ['class' => 'form-control']) !!}

                            </br>
                        </div>

                        <div class="form-group">
                            {!! Form::label('type', 'Vehicle Type:') !!}
                            {!! Form::select('type', ['' => '', 'Cars' => 'Cars', 'Vans' => 'Vans', 'SUVs' => 'SUVs' ], $data->type) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('size', 'Vehicle Size:') !!}
                            {!! Form::select('size', ['' => '','Small' => 'Small', 'Medium' => 'Medium', 'Large' => 'Large'], $data->size) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('doors', 'No. Doors:') !!}
                            {!! Form::select('doors', ['' => '','2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8'], $data->doors) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('capacity', 'Passenger Capacity:') !!}
                            {!! Form::select('capacity', ['' => '','2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10'], $data->capacity) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('transmission', 'Transmission Type:') !!}
                            {!! Form::select('transmission', ['' => '','Manual' => 'Manual', 'Automatic' => 'Automatic'], $data->transmission) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('colour', 'Vehicle Colour:') !!}
                             {!! Form::select('colour', ['' => '','Red' => 'Red', 'Blue' => 'Blue', 'Yellow' => 'Yellow', 'Green' => 'Green', 'Orange' => 'Orange',
                            'Black' => 'Black', 'White' => 'White', 'Grey' => 'Grey'], $data->colour) !!}
                        </div>
                
        
                </div>
                {{ Form::submit('UPDATE VEHICLE', array('class' => 'btn btn-info')) }}
                </br>
                {!! Form::close() !!}
                <br></br>

                </div>


        </div> 
      </div>
    </div>
                          <div align="center"><br>  
                                <button class="btn btn-primary" onclick="history.go(-1)">
                                    « Return Back
                              </button>
                        </div>
</div>
@endsection
