@extends('layouts.main')

@section('content')

    <div class="row" align="center">

        <div class="col-md-10 col-md-offset-1">



        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
            @section('contentheader')
           <h2><i class="fa fa-info-circle"></i> Rent Details </h2>
           @endsection
                <div class="panel-body">
                 

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if(Session::has('successrequest'))
                            <div class="alert alert-success">
                                {{ Session::get('successrequest') }}
                            </div>
                        @endif

                   

         
                        <!-- Content form input -->
                      
    {!! Form::open() !!}
                        <div class="form-group">
                           {!! Form::label('rentername', 'New Renter Name:') !!}
                           {!! Form::select('rentername', $Customers, null) !!}

                        </div>
                          <div>-------------------------------------------------------------------------</div>
                   <br></br>
                        <div class="form-group">
                          {!! Form::label('currentpaymenttype', 'Current Payment Type:') !!}
                          {!! Form::label('currentpaymenttype', $data->payment_type) !!}
                           <br></br>

                           {!! Form::label('paymenttype', 'New Payment type:') !!}
                           {!! Form::select('paymenttype', ['Choose an option' => "", 'Daily' => 'Daily', 'Weekly' => 'Weekly', 'Monthly' => 'Monthly']) !!}
                    <br></br>
                          <div>-------------------------------------------------------------------------</div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('rent_start', 'Rent Start:') !!}
                           {!! Form::text('rent_start', $data->rent_start, ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('rent_end', 'Rent End:') !!}
                            {!! Form::text('rent_end', $data->rent_end, ['class' => 'form-control']) !!}
                        </div>


                
        
                </div>
                {{ Form::submit('UPDATE RENT', array('class' => 'btn btn-info')) }}
                </br>
                {!! Form::close() !!}
                <br></br>

                </div>

        </div> 
      </div>


</div>
@endsection
