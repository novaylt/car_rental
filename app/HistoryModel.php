<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryModel extends Model
{
     protected $table = 'renthistory';
   protected $fillable = array('rented_vehicle_id', 'renter_id', 'renter_name', 'payment_type', 'rent_start' ,'rent_end', 'created_at');
}
