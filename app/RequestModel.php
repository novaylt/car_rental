<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestModel extends Model
{
     protected $table = 'requests';
     protected $fillable = array('requester_id', 'type', 'doors', 'capacity', 'transmission', 'colour', 'created_at');
}
