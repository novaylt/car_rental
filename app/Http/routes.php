<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::group(['middleware' => 'web'], function () {
            Route::group(['middleware' => 'admin'], function () {


                // Manage Vehicles routes, connected controller and methods
                    Route::get('managevehicles', function () {
                    return view('managevehicles');
                    });

                    Route::get('managevehicles', 'VehicleController@GetAllVehicles');
                    Route::delete('managevehicles/delete/{id}',array('uses' => 'VehicleController@destroy', 'as' => 'managevehicles'));
                    Route::get('managevehicles/edit/{id}',array('uses' => 'VehicleController@edit', 'as' => 'editvehicle'));
                    Route::post('managevehicles/edit/{id}',array('uses' => 'VehicleController@update', 'as' => 'editvehicle'));
                    Route::get('managevehicles/editrent/{rentstatusid}',array('uses' => 'VehicleController@editrent', 'as' => 'editrent'));
                    Route::post('managevehicles/editrent/{rentstatusid}',array('uses' => 'VehicleController@updaterent', 'as' => 'editrent'));
                    Route::get('managevehicles/editrentstatus/{id}/{rentedvehicleid}', 'VehicleController@repossescar');


                // Manage Messages routes, connected controller and methods
                    Route::get('messages', function () {
                            return view('messages');
                        });

                    Route::get('messages/{id}', 'ContactController@showmessagecontents');
                    Route::get('messages', 'ContactController@GetMessages');
                    Route::delete('messages/delete/{id}',array('uses' => 'ContactController@destroy', 'as' => 'messages'));




                // Manage Requests route, connected controller and methods
                    Route::get('recieverequests', function () {
                    return view('recieverequests');
                    });


                    Route::get('recieverequests', 'RequestsController@GetRequests');
                    Route::get('recieverequests/{id}', 'RequestsController@showrequestcontents');
                    Route::delete('recieverequests/delete/{id}',array('uses' => 'RequestsController@destroy', 'as' => 'recieverequests'));
                    Route::get('searchresults', array('uses' => 'RequestsController@search', 'as' => 'searchresults'));


                // Retrieving and displaying vehicles    
                    Route::get('vehicles', function () {
                            return view('vehicles');
                        });

                    Route::get('vehicles', 'VehicleController@GetVehicles');


                // Adding new books to the database route
                    Route::get('addvehicle', function () {
                    return view('addvehicle');
                    });

                    Route::post('vehicles', 'VehicleController@store');



                // Manage Users routes, connected controller and methods
                    Route::get('manageusers', function () {
                    return view('manageusers');
                    });

                    Route::get('manageusers', 'UserController@GetUsers');
                    Route::get('manageusers/ban/{id}',array('uses' => 'UserController@ban', 'as' => 'manageusers'));
                    Route::get('manageusers/unban/{id}',array('uses' => 'UserController@unban', 'as' => 'manageusers'));
                    Route::delete('manageusers/delete/{id}',array('uses' => 'UserController@destroy', 'as' => 'manageusers'));


                // Manage Discussions routes, connected controller and methods
                    Route::get('discussionslist', function () {
                            return view('discussionslist');
                        });

                    Route::get('discussionslist', 'DiscussionController@GetDiscussions');
                    Route::get('discussionslist/{id}',array('uses' => 'DiscussionController@showcustomerthreads', 'as' => 'discussionslist'));
                    Route::post('discussionslist', 'DiscussionController@createadminreply');


                // Manage Car Rent route, connected controller and methods
                    Route::get('rentcar', function () {
                    return view('rentcar');
                    });

                   // Route::get('rentcar', 'VehicleController@GetCustomers');
                   Route::get('rentcar/{id}',array('uses' => 'VehicleController@rentcar', 'as' => 'rentcar'));
                   Route::post('rentcar/{id}',array('uses' => 'VehicleController@storerent', 'as' => 'rentcar'));


       });

Route::auth();

// Display Dashboard and statistics
Route::get('dashboard', function () {
    return view('dashboard');
})->middleware(['auth']);

Route::get('dashboard', 'StatisticsController@GetStatistics');



// Display Contact page and store contact message
Route::get('contact', function () {
    return view('contact');
});

Route::post('contact', 'ContactController@store');



// Customer/ Admin discussions routes, connected controller and methods
Route::get('customerservice', function () {
        return view('customerservice');
    })->middleware(['auth', 'ban']);

Route::get('customerservice', 'DiscussionController@showcustomerservice')->middleware(['auth', 'ban']);
Route::get('customerservice/{id}',array('uses' => 'DiscussionController@showadminthread', 'as' => 'customerservice'))->middleware(['auth', 'ban']);
Route::post('customerservice', 'DiscussionController@createcustomerreply');




// Profile route, connected controllers and methods
Route::get('/profile',array('uses' => 'ProfileController@show', 'as' => 'profile'))->middleware(['auth']);
Route::post('/profile',array('uses' => 'ProfileController@update', 'as' => 'profile'));
Route::get('/profile/{id}',array('uses' => 'ProfileController@showProfile', 'as' => 'profile'));



// Request route, connected controllers and methods
Route::get('/request', function () {
return view('request');
})->middleware(['auth', 'ban']);

Route::post('request', 'RequestsController@store');

Route::get('/', 'HomeController@index');

});

