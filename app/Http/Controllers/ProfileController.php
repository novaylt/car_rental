<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Input;
use Redirect;
use Session;
use DB;
use Auth;
use Hash;

class ProfileController extends Controller
{

// Method for retrieving user profile details  
       public function show()
    {
            $profileinfo = User::find(Auth::user()->id);
             return view('profile')->with ('profileinfo', $profileinfo);
    }


// Method for updating user password
      public function update(Request $request)
    {
      $user = User::find(Auth::user()->id);
      $user->password   = Hash::make(Input::get('password'));
      $user->save();
        Session::flash('profile_updated_message', 'Profile Updated!');
        return Redirect::to('profile');
    }

}
