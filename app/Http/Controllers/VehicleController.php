<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VehicleModel;
use App\HistoryModel;
use App\RentModel;
use App\Http\Requests;
use Intervention\Image\ImageManager;
use Validator;
use Input;
use Redirect;
use Session;
use DB;
use Image;
use Auth;
use Hash;

class VehicleController extends Controller
{

// Method for retrieving list of vehicles  
    public function GetVehicles(){
    $Vehicles =  DB::table('vehicles')
             ->select('vehicles.id', 'vehicles.name as name', 'vehicles.type as type', 'vehicles.size as size', 'vehicles.doors as doors', 
              'vehicles.capacity as capacity', 'vehicles.transmission as transmission', 'vehicles.colour as colour', 'vehicles.daily_rate as dailyrate',  
              'vehicles.weekly_rate as weeklyrate', 'vehicles.monthly_rate as monthlyrate', 'vehicles.status as status')
             ->orderBy('vehicles.id')          
             ->paginate(10);
    $Renter =  DB::table('rentstatus')
             ->select('users.id as userid', 'users.name as rentername', 'users.email as renteremail', 'users.created_at as joinedat', 'rentstatus.rented_vehicle_id as rentedvehicleid', 'rentstatus.renter_id as renterid',
              'rentstatus.created_at as rented_at', 'rentstatus.payment_type as paymenttype','rentstatus.rent_start as rentstart','rentstatus.rent_end as rentend','vehicles.name as vehiclename')
             ->join('users', 'users.id', '=', 'rentstatus.renter_id') 
             ->join('vehicles', 'vehicles.id', '=', 'rentstatus.rented_vehicle_id')          
             ->get();
            
    return view('vehicles')
        ->with('Vehicles', $Vehicles)
        ->with('Renter', $Renter);
  }


// Method for storing a new vehicle
  public function store(Request $request)
  {
          $rules = array(

        'cover' => 'required|image|mimes:jpeg,png,jpg|max:2048',
 		    'name' => 'required',
        'type' => 'required',
        'size' => 'required',
        'doors' => 'required',
        'capacity' => 'required',
        'transmission' => 'required',
        'daily_rate' => 'required',
        'weekly_rate' => 'required',
        'monthly_rate' => 'required',

      );

      $validator = Validator::make(Input::all(), $rules);

        if ($validator-> fails())
            {
              return redirect('addvehicle')
              ->withErrors($validator)
              ->withInput();
            }
        else
            {
              $class = new \App\VehicleModel;
              $class->name = Input::get('name');
              $class->type = Input::get('type');
              $class->size = Input::get('size');
              $class->doors = Input::get('doors');
              $class->capacity = Input::get('capacity');
              $class->transmission = Input::get('transmission');
              $class->colour = Input::get('colour');
              $class->daily_rate = Input::get('daily_rate');
              $class->weekly_rate = Input::get('weekly_rate');
              $class->monthly_rate = Input::get('monthly_rate');
              $class -> save();


            $image = $request->file('cover');
            $input['cover'] = $class->id.'.'.$image->getClientOriginalExtension();
            
             $destinationPath = public_path('uploads/');
              $img = Image::make($image->getRealPath());
              $img->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$input['cover']);

              $destinationPath = public_path('images/');
              $image->move($destinationPath, $input['cover']);



           Session::flash('vehicleaddedmessage', 'Vehicle Added!');
           return Redirect::to('vehicles');
        }
    }



// Method for retrieving and showing vehicles on managevehicles view
public function GetAllVehicles(){
    $ManageVehicles =  DB::table('vehicles')
             ->select('vehicles.id', 'vehicles.name as vehiclename', 'vehicles.type as type', 'vehicles.size as size', 'vehicles.doors as doors', 'vehicles.capacity as capacity', 'vehicles.transmission as transmission', 'vehicles.colour as colour', 'vehicles.daily_rate as dailyrate',  'vehicles.weekly_rate as weeklyrate', 'vehicles.monthly_rate as monthlyrate', 'vehicles.status as status')
             ->orderBy('vehicles.id')  
             ->paginate(10);
    $ViewRenter =  DB::table('rentstatus')
             ->select('users.id as userid', 'users.name as rentername', 'users.email as renteremail', 'users.created_at as joinedat', 'rentstatus.id as rentstatusid', 'rentstatus.rented_vehicle_id as rentedvehicleid', 'rentstatus.renter_id as renterid',  'rentstatus.payment_type as paymenttype', 'rentstatus.rent_start as rentstart','rentstatus.rent_end as rentend', 'rentstatus.created_at as rented_at', 'vehicles.name as vehiclename')
             ->join('users', 'users.id', '=', 'rentstatus.renter_id') 
             ->join('vehicles', 'vehicles.id', '=', 'rentstatus.rented_vehicle_id')          
             ->get();
            
    return view('managevehicles')
        ->with('ManageVehicles', $ManageVehicles)
        ->with('ViewRenter', $ViewRenter);
}


// Method for editing vehciles on editvehicles view
public function edit($id)
{
  $data = VehicleModel::findOrFail($id);
  return view('editvehicle')->with('data', $data);
}


// Method for repossessing a rented vehicle
public function repossescar($id, $rentedvehicleid)
{

          RentModel::destroy($id);

          $repossesstatus = VehicleModel::find($rentedvehicleid);
          $repossesstatus->status = 0;
          $repossesstatus -> save();


          Session::flash('repossesstatus', 'Car Has Been Repossed!');
          return Redirect::back();
}



// Method for updating vehicles on editvehicles view
public function update($id, Request $request)
{
   $data = VehicleModel::findOrFail($id);
   $this->validate($request, [
        
        'name' => 'required',
        'type' => 'required',
        'size' => 'required',
        'doors' => 'required',
        'capacity' => 'required',
        'transmission' => 'required',
        'colour' => 'required',

    ]);

    $input = $request->all();
    $data->fill($input)->save();


    Session::flash('vehicle_update_message', 'Vehicle Updated!');
    return Redirect::to('managevehicles');
  }




// Method for editing rent of a vehicle on editrent view
public function editrent($rentstatusid)
{

  $data = RentModel::find($rentstatusid);
  $Customers = \App\User::where('users.admin' ,'=' ,'0')->pluck('name', 'id');
     return view('editrent')
              ->with('data', $data)
              ->with('Customers',$Customers);
}




// Method for updating vehicles rent details on editrent view
public function updaterent($rentstatusid, Request $request)
{
   $class = RentModel::findOrFail($rentstatusid);
   $this->validate($request, [
        
        'rentername' => 'required',
        'paymenttype' => 'required',
        'rent_start' => 'required',
        'rent_end' => 'required',

    ]);

    $class->renter_id = Input::get('rentername');
    $class->payment_type = Input::get('paymenttype');
    $class->rent_start = Input::get('rent_start');
    $class->rent_end = Input::get('rent_end');
    $class -> save();



    Session::flash('rent_update_message', 'Rent Updated!');
    return Redirect::to('managevehicles');
  }





// Method for deleting vehicles on managevehicle view
    public function destroy($id)
    {
      VehicleModel::destroy($id);

      Session::flash('delete_message', 'Vehicle Deleted!');
      return Redirect::to('managevehicles');
    }





// Method for renting a vehicle
    public function rentcar($id){
    
       $vehicledata = \App\VehicleModel::findOrFail($id);
       $Customers = \App\User::where('users.admin' ,'=' ,'0')->pluck('name', 'id');

      return view('rentcar')
      ->with('vehicledata', $vehicledata)
      ->with('Customers',$Customers);
      
    }



// Method for storing rent details to database
  public function storerent($id, Request $request)
  {
          $rules = array(
          'customer' => 'required',
          'paymenttype' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

       if ($validator-> fails())
              {
                return redirect('rentcar')
                ->withErrors($validator)
                ->withInput();
              }
       else
            {

              $newrenternackup = new \App\HistoryModel;
              $newrenternackup->rented_vehicle_id = $id;                       
              $newrenternackup->renter_id = Input::get('customer'); 
              $newrenternackup->payment_type = Input::get('paymenttype');  
              $newrenternackup->rent_start = Input::get('rent_start');  
              $newrenternackup->rent_end = Input::get('rent_end');  
              $newrenternackup -> save();

              $newrenter = new \App\RentModel;
              $newrenter->rented_vehicle_id = $id;                       
              $newrenter->renter_id = Input::get('customer'); 
              $newrenter->payment_type = Input::get('paymenttype');  
              $newrenter->rent_start = Input::get('rent_start');  
              $newrenter->rent_end = Input::get('rent_end');  
              $newrenter -> save();

              $rentstatus = \App\VehicleModel::findOrFail($id);
              $rentstatus->status = 1; 
              $rentstatus -> save();

              Session::flash('rentsaved', 'Rent Application Saved!');
              return Redirect::to('vehicles');
            } 

    }



}
