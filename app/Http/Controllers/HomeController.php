<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */


// Method for retrieving statistics when admin logs in
    public function index()
    {    
        $TotalUsers = \App\User::count();
        $TotalVehicles = \App\VehicleModel::count();
        $TotalRequests = \App\RequestModel::count();
        $TotalContactMessages = \App\ContactModel::count();
        $RentedCars = \App\VehicleModel::where('vehicles.status' ,'=' ,'1')->count();  
        $AvailableCars = \App\VehicleModel::where('vehicles.status' ,'=' ,'0')->count(); 
        $BannedUsers = \App\User::where('users.ban_status' ,'=' ,'1')->count();  
        $NotBannedUsers = \App\User::where('users.ban_status' ,'=' ,'0')->count();  
        $RentHistory =  DB::table('vehicles')
             ->select('vehicles.id', 'vehicles.name as vehiclename', 'vehicles.type as type', 'vehicles.size as size', 
                'vehicles.doors as doors', 'vehicles.capacity as capacity', 'vehicles.transmission as transmission', 'vehicles.colour as colour', 
                'vehicles.daily_rate as dailyrate',  'vehicles.weekly_rate as weeklyrate', 'vehicles.monthly_rate as monthlyrate', 
                'vehicles.status as status','renthistory.id as rentstatusid', 'renthistory.rented_vehicle_id as rentedvehicleid', 
                'renthistory.renter_id as renterid', 'renthistory.rent_start as rentstart' , 'renthistory.rent_end as rentend', 
                'renthistory.payment_type as paymenttype', 'renthistory.created_at')
             ->orderBy('renthistory.created_at')
             ->where('renthistory.renter_id', '=', Auth::id())               
             ->join('renthistory', 'renthistory.rented_vehicle_id', '=', 'vehicles.id')    
             ->paginate(10);

         return view('/dashboard')
                    ->with('TotalUsers',$TotalUsers)
                    ->with('TotalVehicles',$TotalVehicles)
                    ->with('TotalRequests',$TotalRequests)
                    ->with('TotalContactMessages',$TotalContactMessages)
                    ->with('RentedCars',$RentedCars)
                    ->with('AvailableCars',$AvailableCars)
                    ->with('BannedUsers',$BannedUsers)
                    ->with('NotBannedUsers',$NotBannedUsers)
                    ->with('RentHistory',$RentHistory);

    
       
    }
}
