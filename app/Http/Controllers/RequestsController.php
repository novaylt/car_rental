<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RequestModel;
use App\Http\Requests;
use Validator;
use Input;
use Redirect;
use Session;
use DB;
use Auth;

class RequestsController extends Controller
{


// Method for searching and comparing customer car request with vehicle database 
public function search(Request $request)
{

    $type = Input::get('typequery');
    $doors = Input::get('doorsquery');
    $capacity = Input::get('capacityquery');
    $transmission = Input::get('transmissionquery');
    $colour = Input::get('colourquery');
    $Renter =  DB::table('rentstatus')
             ->select('users.id as userid', 'users.name as rentername', 'users.email as renteremail', 'users.created_at as joinedat', 'rentstatus.rented_vehicle_id as rentedvehicleid', 'rentstatus.renter_id as renterid',
              'rentstatus.created_at as rented_at', 'rentstatus.payment_type as paymenttype','rentstatus.rent_start as rentstart','rentstatus.rent_end as rentend','vehicles.name as vehiclename')
             ->join('users', 'users.id', '=', 'rentstatus.renter_id') 
             ->join('vehicles', 'vehicles.id', '=', 'rentstatus.rented_vehicle_id')          
             ->get();

    $searchvehicles = DB::table('vehicles')
        ->where('type', 'LIKE', '%' . $type . '%')
        ->where('doors', 'LIKE', '%' . $doors . '%')
        ->where('capacity', 'LIKE', '%' . $capacity . '%')
        ->where('transmission', 'LIKE', '%' . $transmission . '%')
        ->where('colour', 'LIKE', '%' . $colour . '%')
        ->Paginate(5);

    $countresults = DB::table('vehicles')
        ->where('type', 'LIKE', '%' . $type . '%')
        ->where('doors', 'LIKE', '%' . $doors . '%')
        ->where('capacity', 'LIKE', '%' . $capacity . '%')
        ->where('transmission', 'LIKE', '%' . $transmission . '%')
        ->where('colour', 'LIKE', '%' . $colour . '%')
        ->count();    

     return view('searchresults', compact('Renter', 'searchvehicles', 'type', 'countresults'));
 }




// Method for storing customer vehicle request  
  public function store()
  {
      $rules = array(

        'type' => 'required',
        'doors' => 'required',
        'capacity' => 'required',
        'transmission' => 'required',
        'colour' => 'required',

      );

      $validator = Validator::make(Input::all(), $rules);

        if ($validator-> fails())
            {
              return redirect('request')
              ->withErrors($validator)
              ->withInput();
            }
        else
            {
              $class = new \App\RequestModel;
              $class->requester_id = Auth::user()->id;
              $class->type = Input::get('type');
              $class->doors = Input::get('doors');
              $class->capacity = Input::get('capacity');
              $class->transmission = Input::get('transmission');
              $class->colour = Input::get('colour');
              $class -> save();

              Session::flash('successrequest', 'Request Sent!');
              return Redirect::to('request');
            }
    }


// Method for retrieving requests 
    public function GetRequests(){
      $CarRequest = DB::table('requests')     
        ->select('users.name as requestername', 'users.email as requesteremail', 'requests.requester_id','requests.id', 'requests.created_at')    
        ->join('users', 'users.id', '=', 'requests.requester_id')
        ->get();

      return view('recieverequests')->with('CarRequest', $CarRequest);
    }


// Method for retrieving customer vehicle requests 
    public function showrequestcontents($id)
    {
        $requestinfo = RequestModel::find($id);
        return view('requestcontents')->with ('requestinfo', $requestinfo);
    }



// Method for deleting customer vehicle request
    public function destroy($id)
    {
      RequestModel::destroy($id);
      Session::flash('delete_message', 'Request Deleted Successfully!');
      return Redirect::to('recieverequests');
    }



}
