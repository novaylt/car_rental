<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\User;
use App\Http\Controllers\Controller;
use App\DiscussionsModel;
use App\DiscussionContentsModel;
use Validator;
use Input;
use Auth;
use Image;
use DB;
use Redirect;
use Session;


class DiscussionController extends Controller
{
// Method for retrieving customer service operator on discussion page 
	    public function GetDiscussions(){
          $Discussions = DB::table('users')     
          ->select('users.id', 'users.name as customer_name', 'users.email as customer_email', 'users.created_at as joinedat' )
          ->where('users.admin', '=', 0)                           
          ->orderBy('users.created_at')
          ->paginate(6);


        $TotalDiscussions =   \App\User::where('users.admin' ,'=' ,'0')->count();
	            
	    return view('discussionslist')
	              ->with('Discussions', $Discussions)
	              ->with('TotalDiscussions', $TotalDiscussions);
	  }



// Method for retrieving posts, messages, audio, video and images of a converstaion thread  
	    public function showcustomerthreads($id)
		{
			  Session::put('customerthreadid', $id);

			
	          $threadmessages = DB::table('discussioncontents')     
	          ->select('discussioncontents.id as messageid', 'discussioncontents.customer_thread_id as customerthreadid', 'discussioncontents.sender_id as senderid', 'discussioncontents.message', 
	          	'discussioncontents.created_at', 'discussioncontents.attachment as attachments',  'discussioncontents.audio as audioattachment', 'discussioncontents.video as videoattachment','users.name as sendername')   
	          ->join('users', 'users.id', '=', 'discussioncontents.sender_id')
	          ->where('discussioncontents.customer_thread_id', '=', $id)                
	          ->orderBy('discussioncontents.created_at', 'DESC')
	          ->paginate(5);
	     
			      return view('discussionscontent')
			       		 
	                      ->with ('threadmessages', $threadmessages);
		  }


   

// Method for creating a new discussion topic  
		  public function createadminreply(Request $request)
		  {


     
     	 		  $class = new \App\DiscussionContentsModel;
	              $class->sender_id = Auth::user()->id;
	              $class->customer_thread_id =  Session::get('customerthreadid');
	              $class->message = Input::get('adminreply');


//Image upload request	
			 if ($request->hasFile('image')) {	          
					$class->attachment = "Yes";	
			    }else{
			    	$class->attachment = "";        
			    }

//Audio upload request	
			 if ($request->hasFile('audio')) {	          
					$class->audio = "Yes";	
			    }else{
			    	$class->audio = "";        
			    }

//Video upload request	
			 if ($request->hasFile('video')) {	          
					$class->video = "Yes";	
			    }else{
			    	$class->video = "";        
			    }


 				   $class -> save();

	           
//Image upload, rename and save function	
 				if ($request->hasFile('image')) {	   

				$image = $request->file('image');
				$input['image'] = $class->id.'.'.$image->getClientOriginalExtension();
	            $destinationPath = public_path('attachments/');
	            $img = Image::make($image->getRealPath());
	            $img->resize(1080, 720, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$input['image']);

			    }
			    else{
			    }
	
//Audio upload, rename and save function
				if($request->hasFile('audio'))
					{
					    $music_file = Input::file('audio'); 
					    $input['audio'] = $class->id.'.'.$music_file->getClientOriginalExtension();
						$location = public_path('attachments/'); 
						$music_file->move($location,$input['audio']); 
						
					}
					else{
					}


//Video upload, rename and save function
				if($request->hasFile('video'))
					{
					    $video_file = Input::file('video'); 
					    $input['video'] = $class->id.'.'.$video_file->getClientOriginalExtension();
						$location = public_path('attachments/'); 
						$video_file->move($location,$input['video']); 
						
					}
					else{
					}



			   Session::flash('reply_success','Reply Has Been Submitted!');
			   return Redirect::back();
			     
	 }  





// Method for retrieving list of customers to talk to   
	    public function showcustomerservice()
		{
 				$CustomerServiceThread = DB::table('users')     
		          ->select('users.id', 'users.created_at' )   
		          ->where('users.id', '=', Auth::user()->id)                  
		          ->get();  

		           return view('customerservice')
	                      ->with ('CustomerServiceThread', $CustomerServiceThread);
		    }  		

// Method for retrieving posts, messages, audio, video and images of a converstaion thread  
	    public function showadminthread($id)
		{
		$threadconvo = \App\User::where( 'users.id', '=' , $id);

		 if(Auth::user()->id!=$id) 
		      {
		            return abort(503);
		      }
		     else{
					Session::put('servicethreadid', $id);

			          $threadconvo = DB::table('discussioncontents')     
			          ->select('discussioncontents.id as messageid', 'discussioncontents.customer_thread_id as customerthreadid', 
			          	'discussioncontents.sender_id as senderid', 'discussioncontents.message', 
			          	'discussioncontents.created_at', 'discussioncontents.attachment as attachments',  
			          	'discussioncontents.audio as audioattachment', 'discussioncontents.video as videoattachment', 
			          	'users.name as sendername')   
			          ->join('users', 'users.id', '=', 'discussioncontents.sender_id')
			          ->where('discussioncontents.customer_thread_id', '=', $id)                
			          ->orderBy('discussioncontents.created_at', 'DESC')
			          ->paginate(6);

		 			return view('customerservicecontent')
			                      ->with ('threadconvo', $threadconvo);
				 

		     }

		  }




// Method for creating a new discussion topic  
		  public function createcustomerreply(Request $request)
		  {


     	 		  $class = new \App\DiscussionContentsModel;
	              $class->sender_id = Auth::user()->id;
	              $class->customer_thread_id =  Session::get('servicethreadid');
	              $class->message = Input::get('customerreply');

//Image upload request		
			 if ($request->hasFile('image')) {	          
					$class->attachment = "Yes";	
			    }else{
			    	$class->attachment = "";        
			    }

//Audio upload request	
			 if ($request->hasFile('audio')) {	          
					$class->audio = "Yes";	
			    }else{
			    	$class->audio = "";        
			    }

//Video upload request	
			 if ($request->hasFile('video')) {	          
					$class->video = "Yes";	
			    }else{
			    	$class->video = "";        
			    }


 				   $class -> save();

	           
//Image upload, rename and save function	
 				if ($request->hasFile('image')) {	   

				$image = $request->file('image');
				$input['image'] = $class->id.'.'.$image->getClientOriginalExtension();
	            $destinationPath = public_path('attachments/');
	            $img = Image::make($image->getRealPath());
	            $img->resize(1080, 720, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$input['image']);

			    }
			    else{
			    }
	
//Audio upload, rename and save function	
				if($request->hasFile('audio'))
					{
					    $music_file = Input::file('audio'); 
					    $input['audio'] = $class->id.'.'.$music_file->getClientOriginalExtension();
						$location = public_path('attachments/'); 
						$music_file->move($location,$input['audio']); 
						
					}
					else{
					}


//Video upload, rename and save function	
				if($request->hasFile('video'))
					{
					    $video_file = Input::file('video'); 
					    $input['video'] = $class->id.'.'.$video_file->getClientOriginalExtension();
						$location = public_path('attachments/'); 
						$video_file->move($location,$input['video']); 
						
					}
					else{
					}
	

		   Session::flash('customer_reply_success','Reply Has Been Submitted!');
		   return Redirect::back();
		  }  




}
