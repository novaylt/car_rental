<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use Auth;
use App\Http\Requests;

class StatisticsController extends Controller
{

// Method for retrieving statistics using queries when clicking the Statistics page  
    public function GetStatistics()
    {
		$TotalUsers = \App\User::count();
		$TotalVehicles = \App\VehicleModel::count();
		$TotalRequests = \App\RequestModel::count();
		$TotalContactMessages = \App\ContactModel::count();
        $RentedCars = \App\VehicleModel::where('vehicles.status' ,'=' ,'1')->count();  
        $AvailableCars = \App\VehicleModel::where('vehicles.status' ,'=' ,'0')->count();  
        $BannedUsers = \App\User::where('users.ban_status' ,'=' ,'1')->count();  
        $NotBannedUsers = \App\User::where('users.ban_status' ,'=' ,'0')->count();
        $TotalChatMessages = \App\DiscussionContentsModel::count();
	    $RentHistory =  DB::table('vehicles')
	             ->select('vehicles.id', 'vehicles.name as vehiclename', 'vehicles.type as type', 'vehicles.size as size', 
	                'vehicles.doors as doors', 'vehicles.capacity as capacity', 'vehicles.transmission as transmission', 'vehicles.colour as colour', 
	                'vehicles.daily_rate as dailyrate',  'vehicles.weekly_rate as weeklyrate', 'vehicles.monthly_rate as monthlyrate', 
	                'vehicles.status as status','renthistory.id as rentstatusid', 'renthistory.rented_vehicle_id as rentedvehicleid', 
	                'renthistory.renter_id as renterid', 'renthistory.rent_start as rentstart' , 'renthistory.rent_end as rentend', 
	                'renthistory.payment_type as paymenttype', 'renthistory.created_at')
	             ->orderBy('renthistory.created_at', 'DESC')
	             ->where('renthistory.renter_id', '=', Auth::id())               
	             ->join('renthistory', 'renthistory.rented_vehicle_id', '=', 'vehicles.id')    
	             ->paginate(10);

		 return view('/dashboard')
			  		->with('TotalUsers',$TotalUsers)
			  		->with('TotalVehicles',$TotalVehicles)
			  		->with('TotalRequests',$TotalRequests)
			  		->with('TotalContactMessages',$TotalContactMessages)
			  		->with('RentedCars',$RentedCars)
			  		->with('AvailableCars',$AvailableCars)
			  		->with('BannedUsers',$BannedUsers)
                    ->with('NotBannedUsers',$NotBannedUsers)
                    ->with('TotalChatMessages',$TotalChatMessages)
			  		->with('RentHistory',$RentHistory);

    }
}
