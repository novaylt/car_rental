<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleModel extends Model
{
    protected $table = 'vehicles';
     protected $fillable = array('id', 'name', 'type', 'size', 'doors', 'capacity', 'transmission', 'colour', 'status', 'daily_rate', 'weekly_rate', 'monthly_rate', 'created_at');
}
