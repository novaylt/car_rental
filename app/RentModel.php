<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RentModel extends Model
{
   protected $table = 'rentstatus';
   protected $fillable = array('rented_vehicle_id', 'renter_id', 'renter_name', 'payment_type', 'rent_start' ,'rent_end', 'created_at');
}
