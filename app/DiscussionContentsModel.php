<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscussionContentsModel extends Model
{
   protected $table = 'discussioncontents';
   protected $fillable = array('id', 'customer_thread_id', 'sender_id', 'message', 'attachment', 'audio', 'video', 'created_at', 'updated_at');
}
